import * as React from "react";
import { styled, createTheme, ThemeProvider } from "@mui/material/styles";
import CssBaseline from "@mui/material/CssBaseline";
import MuiDrawer from "@mui/material/Drawer";
import Box from "@mui/material/Box";
import MuiAppBar from "@mui/material/AppBar";
import Toolbar from "@mui/material/Toolbar";
import List from "@mui/material/List";
import Typography from "@mui/material/Typography";
import Divider from "@mui/material/Divider";
import PowerSettingsNewIcon from "@mui/icons-material/PowerSettingsNew";
import {
  CircularProgress,
  FormControl,
  Icon,
  MenuItem,
  OutlinedInput,
  Select,
  TextField,
  Tooltip,
} from "@mui/material";
import LogoIcon from "../../assets/unitgrid-logo.svg";
import LogoTextIcon from "../../assets/unitgrid-text-logo.svg";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";
import DashboardOutlinedIcon from "@mui/icons-material/DashboardOutlined";
import ChevronRightIcon from "@mui/icons-material/ChevronRight";
import ChevronLeftIcon from "@mui/icons-material/ChevronLeft";
import { Avatar, IconButton, ListItem } from "@mui/material";
import { deepPurple } from "@mui/material/colors";
import BoltIcon from "@mui/icons-material/Bolt";
import QueryStatsIcon from "@mui/icons-material/QueryStats";
import PriorityHighIcon from "@mui/icons-material/PriorityHigh";
import {
  BarChart,
  ResponsiveContainer,
  Bar,
  XAxis,
  YAxis,
  Tooltip as ReChartToolTip,
  Label,
  CartesianGrid,
  Area,
  AreaChart,
} from "recharts";
import {
  apiResData,
  newResData,
} from "../../data";
import { useLocation, useNavigate } from "react-router-dom";
import moment from "moment";
import axios from "axios";
import {
  baseUrlData,
  projectApiKey,
} from "../../components/constants";
var _ = require("lodash");

const drawerWidth = 240;

const AppBar = styled(MuiAppBar, {
  shouldForwardProp: (prop) => prop !== "open",
})(({ theme, open }) => ({
  zIndex: theme.zIndex.drawer + 1,
  transition: theme.transitions.create(["width", "margin"], {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  ...(open && {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  }),
}));

const Drawer = styled(MuiDrawer, {
  shouldForwardProp: (prop) => prop !== "open",
})(({ theme, open }) => ({
  "& .MuiDrawer-paper": {
    position: "relative",
    whiteSpace: "nowrap",
    width: drawerWidth,
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
    boxSizing: "border-box",
    ...(!open && {
      overflowX: "hidden",
      transition: theme.transitions.create("width", {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
      width: theme.spacing(7),
      [theme.breakpoints.up("sm")]: {
        width: theme.spacing(9),
      },
    }),
  },
}));

const mdTheme = createTheme();

const Dashboard = () => {
  const { pathname } = useLocation();
  let navigate = useNavigate();

  const [areaData, setAreaData] = React.useState([]);
  const [open, setOpen] = React.useState(true);
  const [year, setYear] = React.useState(new Date().getFullYear());
  const [month, setMonth] = React.useState(
    new Date().toLocaleString("default", { month: "long" })
  );
  const [monthData, setMonthData] = React.useState();
  const [totalPowerConsumption, setTotalPowerConsumption] =
    React.useState("0.0Wh");
  const [totalPower, setTotalPower] = React.useState("0.0W");
  const [isTotalPowerLoading, setIsTotalPowerLoading] = React.useState(false);
  const [isPowerConsumptionLoading, setIsPowerConsumptionLoading] =
    React.useState(false);
  const daysInMonth = (monthSelected, yearSelected) => {
    const monthInNumber = monthNames.indexOf(monthSelected) + 1;
    return new Date(yearSelected, monthInNumber, 0).getDate();
  };

  const getNodes = () => {
    return new Promise((resolve, reject) => {
      axios
        .post(
          `${baseUrlData}/v1/node/list`,
          {
            limit: 0,
            offset: 0,
            order: "asc",
          },
          { headers: { Authorization: projectApiKey } }
        )
        .then((response) => {
          resolve(response.data);
        })
        .catch((err) => {
          reject(err);
        });
    });
  };

  const getPercentageIncrease = (numA, numB) => ((numA - numB) / numB) * 100;

  React.useEffect(() => {
    let mainData = [];
    const days = daysInMonth(month, year);
    getNodeDailyConsumption()
      .then(async (res) => {
        if (res?.data) {
          const groupDays = _.groupBy(res?.data, (item) => {
            return moment.unix(item.time).startOf("day").format("DD");
          });
          mainData = [
            {
              name: "01",
              date: 1,
              w: groupDays["01"]?.reduce(
                (p, c) => (c?.importkwh > 0 ? p + c?.importkwh : p),
                0
              ),
            },
            {
              name: "",
              date: 2,
              w: groupDays["02"]?.reduce(
                (p, c) => (c?.importkwh > 0 ? p + c?.importkwh : p),
                0
              ),
            },
            {
              name: "03",
              date: 3,
              w: groupDays["03"]?.reduce(
                (p, c) => (c?.importkwh > 0 ? p + c?.importkwh : p),
                0
              ),
            },
            {
              name: "",
              date: 4,
              w: groupDays["04"]?.reduce(
                (p, c) => (c?.importkwh > 0 ? p + c?.importkwh : p),
                0
              ),
            },
            {
              name: "05",
              date: 5,
              w: groupDays["05"]?.reduce(
                (p, c) => (c?.importkwh > 0 ? p + c?.importkwh : p),
                0
              ),
            },
            {
              name: "",
              date: 6,
              w: groupDays["06"]?.reduce(
                (p, c) => (c?.importkwh > 0 ? p + c?.importkwh : p),
                0
              ),
            },
            {
              name: "07",
              date: 7,
              w: groupDays["07"]?.reduce(
                (p, c) => (c?.importkwh > 0 ? p + c?.importkwh : p),
                0
              ),
            },
            {
              name: "",
              date: 8,
              w: groupDays["08"]?.reduce(
                (p, c) => (c?.importkwh > 0 ? p + c?.importkwh : p),
                0
              ),
            },
            {
              name: "09",
              date: 9,
              w: groupDays["09"]?.reduce(
                (p, c) => (c?.importkwh > 0 ? p + c?.importkwh : p),
                0
              ),
            },
            {
              name: "",
              date: 10,
              w: groupDays["10"]?.reduce(
                (p, c) => (c?.importkwh > 0 ? p + c?.importkwh : p),
                0
              ),
            },
            {
              name: "11",
              date: 11,
              w: groupDays["11"]?.reduce(
                (p, c) => (c?.importkwh > 0 ? p + c?.importkwh : p),
                0
              ),
            },
            {
              name: "",
              date: 12,
              w: groupDays["12"]?.reduce(
                (p, c) => (c?.importkwh > 0 ? p + c?.importkwh : p),
                0
              ),
            },
            {
              name: "13",
              date: 13,
              w: groupDays["13"]?.reduce(
                (p, c) => (c?.importkwh > 0 ? p + c?.importkwh : p),
                0
              ),
            },
            {
              name: "",
              date: 14,
              w: groupDays["14"]?.reduce(
                (p, c) => (c?.importkwh > 0 ? p + c?.importkwh : p),
                0
              ),
            },
            {
              name: "15",
              date: 15,
              w: groupDays["15"]?.reduce(
                (p, c) => (c?.importkwh > 0 ? p + c?.importkwh : p),
                0
              ),
            },
            {
              name: "",
              date: 16,
              w: groupDays["16"]?.reduce(
                (p, c) => (c?.importkwh > 0 ? p + c?.importkwh : p),
                0
              ),
            },
            {
              name: "17",
              date: 17,
              w: groupDays["17"]?.reduce(
                (p, c) => (c?.importkwh > 0 ? p + c?.importkwh : p),
                0
              ),
            },
            {
              name: "",
              date: 18,
              w: groupDays["18"]?.reduce(
                (p, c) => (c?.importkwh > 0 ? p + c?.importkwh : p),
                0
              ),
            },
            {
              name: "19",
              date: 19,
              w: groupDays["19"]?.reduce(
                (p, c) => (c?.importkwh > 0 ? p + c?.importkwh : p),
                0
              ),
            },
            {
              name: "",
              date: 20,
              w: groupDays["20"]?.reduce(
                (p, c) => (c?.importkwh > 0 ? p + c?.importkwh : p),
                0
              ),
            },
            {
              name: "21",
              date: 21,
              w: groupDays["21"]?.reduce(
                (p, c) => (c?.importkwh > 0 ? p + c?.importkwh : p),
                0
              ),
            },
            {
              name: "",
              date: 22,
              w: groupDays["22"]?.reduce(
                (p, c) => (c?.importkwh > 0 ? p + c?.importkwh : p),
                0
              ),
            },
            {
              name: "23",
              date: 23,
              w: groupDays["23"]?.reduce(
                (p, c) => (c?.importkwh > 0 ? p + c?.importkwh : p),
                0
              ),
            },
            {
              name: "",
              date: 24,
              w: groupDays["24"]?.reduce(
                (p, c) => (c?.importkwh > 0 ? p + c?.importkwh : p),
                0
              ),
            },
            {
              name: "25",
              date: 25,
              w: groupDays["25"]?.reduce(
                (p, c) => (c?.importkwh > 0 ? p + c?.importkwh : p),
                0
              ),
            },
            {
              name: "",
              date: 26,
              w: groupDays["26"]?.reduce(
                (p, c) => (c?.importkwh > 0 ? p + c?.importkwh : p),
                0
              ),
            },
            {
              name: "27",
              date: 27,
              w: groupDays["27"]?.reduce(
                (p, c) => (c?.importkwh > 0 ? p + c?.importkwh : p),
                0
              ),
            },
            {
              name: "",
              date: 28,
              w: groupDays["28"]?.reduce(
                (p, c) => (c?.importkwh > 0 ? p + c?.importkwh : p),
                0
              ),
            },
            {
              name: "29",
              date: 29,
              w: groupDays["29"]?.reduce(
                (p, c) => (c?.importkwh > 0 ? p + c?.importkwh : p),
                0
              ),
            },
            {
              name: "",
              date: 30,
              w: groupDays["30"]?.reduce(
                (p, c) => (c?.importkwh > 0 ? p + c?.importkwh : p),
                0
              ),
            },
            {
              name: "31",
              date: 31,
              w: groupDays["31"]?.reduce(
                (p, c) => (c?.importkwh > 0 ? p + c?.importkwh : p),
                0
              ),
            },
          ];
          const getTotalPowerConsumption = res?.data?.reduce((p, c) => {
            if (c?.importkwh > 0) {
              return p + c?.importkwh;
            } else {
              return p;
            }
          }, 0);
          setTotalPowerConsumption(
            convertToMegaOrKilo(getTotalPowerConsumption || 0.0, "Wh")
          );
          getPercentageIncrease();
          setMonthData(mainData?.slice(0, days));
          //   let nodes = [];
          //   await getNodes()
          //     .then((res) => {
          //       nodes = res?.nodes;
          //     })
          //     .catch((err) => console.log(err));
          //   const reqBodyForPrev = {
          //     nodes,
          //     from: Math.floor(
          //       getFirstDayOfPreviousMonth(year, month).getTime() / 1000
          //     ),
          //     to: Math.floor(
          //       getLastDayOfPreviousMonth(year, month).getTime() / 1000
          //     ),
          //     limit: 0,
          //   };
          //   axios
          //     .post(
          //       `${baseUrlData}/v1/node/getNodeDailyConsumption`,
          //       JSON.stringify(reqBodyForPrev),
          //       { headers: { Authorization: projectApiKey } }
          //     )
          //     .then((response) => {
          //       console.log(response?.data?.data);
          //       if (response?.data?.data) {
          //         const getTotalPowerConsumptionOfPreviousMonth =
          //           response.data?.data?.reduce((p, c) => p + c?.importkwh, 0);
          //       }
          //     })
          //     .catch((err) => {
          //       console.log(err);
          //     });
        }
      })
      .catch((err) => console.log(err));
  }, [year, month]);
  React.useEffect(() => {
    getDevicedHistorical()
      .then((res) => {
        // if (res?.data) {
        const precision = 60 * 60 * 1000;
        const groupHours = _.groupBy(newResData?.data, (item) => {
          const floor =
            Math.floor(moment.unix(item?.time).valueOf() / precision) *
            precision;
          return moment(floor).startOf("hour").format("HH:mm");
        });
        console.log("historicalData?.historical::", newResData?.data);
        const dataToShow = [
          {
            name: "12 am",
            time:
              groupHours["00:00"]?.length > 0
                ? moment.unix(groupHours["00:00"][0]?.time).format("hh:mm A")
                : "12:00 am",
            w:
              groupHours["00:00"]?.length > 0
                ? groupHours["00:00"][0]?.w1 +
                  groupHours["00:00"][0]?.w2 +
                  groupHours["00:00"][0]?.w3
                : 0,
          },
          {
            name: "",
            time:
              groupHours["01:00"]?.length > 0
                ? moment.unix(groupHours["01:00"][0]?.time).format("hh:mm A")
                : "1:00 am",
            w:
              groupHours["01:00"]?.length > 0
                ? groupHours["01:00"][0]?.w1 +
                  groupHours["01:00"][0]?.w2 +
                  groupHours["01:00"][0]?.w3
                : 0,
          },
          {
            name: "",
            time:
              groupHours["02:00"]?.length > 0
                ? moment.unix(groupHours["02:00"][0]?.time).format("hh:mm A")
                : "2:00 am",
            w:
              groupHours["02:00"]?.length > 0
                ? groupHours["02:00"][0]?.w1 +
                  groupHours["02:00"][0]?.w2 +
                  groupHours["02:00"][0]?.w3
                : 0,
          },
          {
            name: "",
            time:
              groupHours["03:00"]?.length > 0
                ? moment.unix(groupHours["03:00"][0]?.time).format("hh:mm A")
                : "3:00 am",
            w:
              groupHours["03:00"]?.length > 0
                ? groupHours["03:00"][0]?.w1 +
                  groupHours["03:00"][0]?.w2 +
                  groupHours["03:00"][0]?.w3
                : 0,
          },
          {
            name: "",
            time:
              groupHours["04:00"]?.length > 0
                ? moment.unix(groupHours["04:00"][0]?.time).format("hh:mm A")
                : "4:00 am",
            w:
              groupHours["04:00"]?.length > 0
                ? groupHours["04:00"][0]?.w1 +
                  groupHours["04:00"][0]?.w2 +
                  groupHours["04:00"][0]?.w3
                : 0,
          },
          {
            name: "",
            time:
              groupHours["05:00"]?.length > 0
                ? moment.unix(groupHours["05:00"][0]?.time).format("hh:mm A")
                : "5:00 am",
            w:
              groupHours["05:00"]?.length > 0
                ? groupHours["05:00"][0]?.w1 +
                  groupHours["05:00"][0]?.w2 +
                  groupHours["05:00"][0]?.w3
                : 0,
          },
          {
            name: "06 am",
            time:
              groupHours["06:00"]?.length > 0
                ? moment.unix(groupHours["06:00"][0]?.time).format("hh:mm A")
                : "6:00 am",
            w:
              groupHours["06:00"]?.length > 0
                ? groupHours["06:00"][0]?.w1 +
                  groupHours["06:00"][0]?.w2 +
                  groupHours["06:00"][0]?.w3
                : 0,
          },
          {
            name: "",
            time:
              groupHours["07:00"]?.length > 0
                ? moment.unix(groupHours["07:00"][0]?.time).format("hh:mm A")
                : "7:00 am",
            w:
              groupHours["07:00"]?.length > 0
                ? groupHours["07:00"][0]?.w1 +
                  groupHours["07:00"][0]?.w2 +
                  groupHours["07:00"][0]?.w3
                : 0,
          },
          {
            name: "",
            time:
              groupHours["08:00"]?.length > 0
                ? moment.unix(groupHours["08:00"][0]?.time).format("hh:mm A")
                : "8:00 am",
            w:
              groupHours["08:00"]?.length > 0
                ? groupHours["08:00"][0]?.w1 +
                  groupHours["08:00"][0]?.w2 +
                  groupHours["08:00"][0]?.w3
                : 0,
          },
          {
            name: "",
            time:
              groupHours["09:00"]?.length > 0
                ? moment.unix(groupHours["09:00"][0]?.time).format("hh:mm A")
                : "9:00 am",
            w:
              groupHours["09:00"]?.length > 0
                ? groupHours["09:00"][0]?.w1 +
                  groupHours["09:00"][0]?.w2 +
                  groupHours["09:00"][0]?.w3
                : 0,
          },
          {
            name: "",
            time:
              groupHours["10:00"]?.length > 0
                ? moment.unix(groupHours["10:00"][0]?.time).format("hh:mm A")
                : "10:00 am",
            w:
              groupHours["10:00"]?.length > 0
                ? groupHours["10:00"][0]?.w1 +
                  groupHours["10:00"][0]?.w2 +
                  groupHours["10:00"][0]?.w3
                : 0,
          },
          {
            name: "",
            time:
              groupHours["11:00"]?.length > 0
                ? moment.unix(groupHours["11:00"][0]?.time).format("hh:mm A")
                : "11:00 am",
            w:
              groupHours["11:00"]?.length > 0
                ? groupHours["11:00"][0]?.w1 +
                  groupHours["11:00"][0]?.w2 +
                  groupHours["11:00"][0]?.w3
                : 0,
          },
          {
            name: "12 pm",
            time:
              groupHours["12:00"]?.length > 0
                ? moment.unix(groupHours["12:00"][0]?.time).format("hh:mm A")
                : "12:00 pm",
            w:
              groupHours["12:00"]?.length > 0
                ? groupHours["12:00"][0]?.w1 +
                  groupHours["12:00"][0]?.w2 +
                  groupHours["12:00"][0]?.w3
                : 0,
          },
          {
            name: "",
            time:
              groupHours["13:00"]?.length > 0
                ? moment.unix(groupHours["13:00"][0]?.time).format("hh:mm A")
                : "1:00 pm",
            w:
              groupHours["13:00"]?.length > 0
                ? groupHours["13:00"][0]?.w1 +
                  groupHours["13:00"][0]?.w2 +
                  groupHours["13:00"][0]?.w3
                : 0,
          },
          {
            name: "",
            time:
              groupHours["14:00"]?.length > 0
                ? moment.unix(groupHours["14:00"][0]?.time).format("hh:mm A")
                : "2:00 pm",
            w:
              groupHours["14:00"]?.length > 0
                ? groupHours["14:00"][0]?.w1 +
                  groupHours["14:00"][0]?.w2 +
                  groupHours["14:00"][0]?.w3
                : 0,
          },
          {
            name: "",
            time:
              groupHours["15:00"]?.length > 0
                ? moment.unix(groupHours["15:00"][0]?.time).format("hh:mm A")
                : "3:00 pm",
            w:
              groupHours["15:00"]?.length > 0
                ? groupHours["15:00"][0]?.w1 +
                  groupHours["15:00"][0]?.w2 +
                  groupHours["15:00"][0]?.w3
                : 0,
          },
          {
            name: "",
            time:
              groupHours["16:00"]?.length > 0
                ? moment.unix(groupHours["16:00"][0]?.time).format("hh:mm A")
                : "4:00 pm",
            w:
              groupHours["16:00"]?.length > 0
                ? groupHours["16:00"][0]?.w1 +
                  groupHours["16:00"][0]?.w2 +
                  groupHours["16:00"][0]?.w3
                : 0,
          },
          {
            name: "",
            time:
              groupHours["17:00"]?.length > 0
                ? moment.unix(groupHours["17:00"][0]?.time).format("hh:mm A")
                : "5:00 pm",
            w:
              groupHours["17:00"]?.length > 0
                ? groupHours["17:00"][0]?.w1 +
                  groupHours["17:00"][0]?.w2 +
                  groupHours["17:00"][0]?.w3
                : 0,
          },
          {
            name: "06 pm",
            time:
              groupHours["18:00"]?.length > 0
                ? moment.unix(groupHours["18:00"][0]?.time).format("hh:mm A")
                : "6:00 pm",
            w:
              groupHours["18:00"]?.length > 0
                ? groupHours["18:00"][0]?.w1 +
                  groupHours["18:00"][0]?.w2 +
                  groupHours["18:00"][0]?.w3
                : 0,
          },
          {
            name: "",
            time:
              groupHours["19:00"]?.length > 0
                ? moment.unix(groupHours["19:00"][0]?.time).format("hh:mm A")
                : "7:00 pm",
            w:
              groupHours["19:00"]?.length > 0
                ? groupHours["19:00"][0]?.w1 +
                  groupHours["19:00"][0]?.w2 +
                  groupHours["19:00"][0]?.w3
                : 0,
          },
          {
            name: "",
            time:
              groupHours["20:00"]?.length > 0
                ? moment.unix(groupHours["20:00"][0]?.time).format("hh:mm A")
                : "8:00 pm",
            w:
              groupHours["20:00"]?.length > 0
                ? groupHours["20:00"][0]?.w1 +
                  groupHours["20:00"][0]?.w2 +
                  groupHours["20:00"][0]?.w3
                : 0,
          },
          {
            name: "",
            time:
              groupHours["21:00"]?.length > 0
                ? moment.unix(groupHours["21:00"][0]?.time).format("hh:mm A")
                : "9:00 pm",
            w:
              groupHours["21:00"]?.length > 0
                ? groupHours["21:00"][0]?.w1 +
                  groupHours["21:00"][0]?.w2 +
                  groupHours["21:00"][0]?.w3
                : 0,
          },
          {
            name: "",
            time:
              groupHours["22:00"]?.length > 0
                ? moment.unix(groupHours["22:00"][0]?.time).format("hh:mm A")
                : "10:00 pm",
            w:
              groupHours["22:00"]?.length > 0
                ? groupHours["22:00"][0]?.w1 +
                  groupHours["22:00"][0]?.w2 +
                  groupHours["22:00"][0]?.w3
                : 0,
          },
          {
            name: "",
            time:
              groupHours["23:00"]?.length > 0
                ? moment.unix(groupHours["23:00"][0]?.time).format("hh:mm A")
                : "11:00 pm",
            w:
              groupHours["23:00"]?.length > 0
                ? groupHours["23:00"][0]?.w1 +
                  groupHours["23:00"][0]?.w2 +
                  groupHours["23:00"][0]?.w3
                : 0,
          },
          {
            name: "12 am",
            time: "12:00 am",
          },
        ];
        const getTotalPower = res?.data?.reduce(
          (p, c) => p + c?.w1 + c?.w2 + c?.w3,
          0
        );

        setTotalPower(convertToMegaOrKilo(getTotalPower, "W"));
        setAreaData(dataToShow);
        // }
      })
      .catch((err) => {
        console.log("getDevicedHistorical::", err?.message);
      });
  }, []);

  const years = (startYear) => {
    var currentYear = new Date().getFullYear(),
      years = [];
    startYear = startYear || 1980;
    while (startYear <= currentYear) {
      years.push(startYear++);
    }
    return years;
  };

  const monthNames = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
  ];

  const toggleDrawer = () => {
    setOpen(!open);
  };
  const handleLogout = () => {
    localStorage.clear();
    navigate("/login");
  };

  const getFirstDayOfMonth = (yearSelected, monthSelected) => {
    const monthInNumber = monthNames.indexOf(monthSelected);
    return new Date(yearSelected, monthInNumber, 1);
  };

  const getLastDayOfMonth = (yearSelected, monthSelected) => {
    const monthInNumber = monthNames.indexOf(monthSelected);
    return new Date(yearSelected, monthInNumber + 1, 0);
  };

  const getNodeDailyConsumption = async () => {
    setIsPowerConsumptionLoading(true);
    let nodes = [];
    await getNodes()
      .then((res) => {
        nodes = res?.nodes;
      })
      .catch((err) => console.log(err));
    const reqBody = {
      nodes,
      from: Math.floor(getFirstDayOfMonth(year, month).getTime() / 1000),
      to: Math.floor(getLastDayOfMonth(year, month).getTime() / 1000),
      limit: 0,
    };
    return new Promise((resolve, reject) => {
      axios
        .post(
          `${baseUrlData}/v1/node/getNodeDailyConsumption`,
          JSON.stringify(reqBody),
          { headers: { Authorization: projectApiKey } }
        )
        .then((response) => {
          if (response?.data) {
            resolve(response.data);
            setIsPowerConsumptionLoading(false);
          }
        })
        .catch((err) => {
          reject(err);
          setIsPowerConsumptionLoading(false);
        });
    });
  };

  const getDevicedHistorical = async () => {
    setIsTotalPowerLoading(true);
    let nodes = [];
    await getNodes()
      .then((res) => {
        nodes = res?.nodes;
      })
      .catch((err) => console.log(err));
    const start = new Date();
    start.setHours(0, 0, 0, 0);
    const end = new Date();
    end.setHours(23, 59, 59, 999);

    const reqBody = {
      nodes,
      from: Math.floor(start.getTime() / 1000),
      to: Math.floor(end.getTime() / 1000),
      groupInterval: 60,
      limit: 0,
      order: "desc",
    };
    return new Promise((resolve, reject) => {
      axios
        .post(
          `${baseUrlData}/v1/node/getDevicedHistorical`,
          JSON.stringify(reqBody),
          { headers: { Authorization: projectApiKey } }
        )
        .then((response) => {
          if (response?.data) {
            resolve(response.data);
            setIsTotalPowerLoading(false);
          }
        })
        .catch((err) => {
          reject(err);
          setIsTotalPowerLoading(false);
        });
    });
  };

  const CustomToolTipOfTotalPower = ({ active, payload }) => {
    if (active && payload && payload.length) {
      return (
        <Box
          bgcolor="#FFFFFF"
          border="0.25px solid #D5D5D5 !important"
          borderRadius={"13px"}
          boxShadow="5px 5px 10px rgba(106, 105, 105, 0.15)"
          padding={2}
        >
          <Typography
            fontWeight={400}
            color="#8D8D8D"
            fontSize={10}
            textAlign="center"
          >
            {payload[0]?.payload?.time}
          </Typography>
          <Box display="flex">
            <Typography
              fontWeight={400}
              color="#8D8D8D"
              fontSize={12}
              sx={{ wordWrap: "break-word" }}
              maxWidth="fit-content"
            >
              Total Power
            </Typography>
            <Typography fontWeight={400} color="#971BF9" fontSize={16}>
              {convertToMegaOrKilo(payload[0]?.value, "W")}
            </Typography>
          </Box>
        </Box>
      );
    }

    return null;
  };
  const CustomToolTipOfConsumptionPattern = ({
    active,
    payload,
    color,
  }) => {
    if (active && payload && payload.length) {
      return (
        <Box
          bgcolor="#FFFFFF"
          border="0.25px solid #D5D5D5 !important"
          borderRadius={"13px"}
          boxShadow="5px 5px 10px rgba(106, 105, 105, 0.15)"
          padding={2}
        >
          <Typography
            fontWeight={300}
            color="#8D8D8D"
            fontSize={10}
            textAlign="center"
          >
            {`${payload[0]?.payload?.date} ${month} ${year}`}
          </Typography>
          <Box display="flex" gap={1} alignItems="center">
            <Typography
              fontWeight={400}
              color="#8D8D8D"
              fontSize={12}
              sx={{ wordWrap: "break-word" }}
              maxWidth="fit-content"
            >
              Power Consumed
            </Typography>
            <Typography fontWeight={400} color={color} fontSize={16}>
              {convertToMegaOrKilo(payload[0]?.value, "Wh")}
            </Typography>
          </Box>
        </Box>
      );
    }

    return null;
  };

  const convertToMegaOrKilo = (total, unit) => {
    if (total < 1000) {
      return `${total.toFixed(2)}${unit}`;
    } else if (total > 1000000) {
      return `${(total / 1000000).toFixed(2)}M${unit}`;
    } else if (total > 1000) {
      return `${(total / 1000).toFixed(2)}k${unit}`;
    }
  };





  return (
    <ThemeProvider theme={mdTheme}>
      <Box sx={{ display: "flex" }}>
        <CssBaseline />
        <AppBar
          position="absolute"
          open={open}
          sx={{
            bgcolor: "#fff",
            boxShadow: "0px 0px 5px rgba(176, 176, 176, 0.2)",
          }}
        >
          <Toolbar
            sx={{
              pr: "24px",
              display: "flex",
              justifyContent: "space-between",
              py: 2,
              pl: "0 !important",
            }}
          >
            <Box>
              <Icon
                sx={{
                  height: "29px",
                  width: "64px",
                  ml: 1,
                  ...(open && { display: "none" }),
                }}
              >
                <img src={LogoIcon} alt="Logo Icon" />
              </Icon>
            </Box>
            {/* <Box>
              <Typography
                component="h1"
                color="black"
                fontSize={"24px"}
                fontWeight={700}
              >
                Ahmedabad Street Lights
              </Typography>
            </Box> */}
            <Box width={"40%"}>
              <TextField
                fullWidth
                id="fullWidth"
                hiddenLabel
                placeholder="Search"
                disabled
                sx={{
                  bgcolor: "#FFFFFF",
                  border: 0,
                  "& input": {
                    border: "1px solid rgba(0, 0, 0, 0.1)",
                    borderRadius: "14.5px",
                    height: "1px",
                  },
                  "& fieldset": { border: "none" },
                  ":hover": {
                    "& input": { bgcolor: "rgba(217, 217, 217, 0.3)" },
                  },
                  ":active": {
                    "& input": {
                      bgcolor: "#ffffff",
                    },
                  },
                }}
              />
            </Box>
            <Box>
              {/* <IconButton
                sx={{
                  color: "#000",
                  ":hover": {
                    bgcolor: "white",
                    color: "rgba(27, 83, 249, 1)",
                  },
                  ":active": {
                    bgcolor: "rgba(3, 237, 165, 0.05)",
                    color: "rgba(27, 83, 249, 1)",
                  },
                }}
              >
                <NotificationsNoneIcon />
              </IconButton>
              <IconButton
                sx={{
                  color: "#000",
                  ":hover": {
                    bgcolor: "white",
                    color: "rgba(27, 83, 249, 1)",
                  },
                  ":active": {
                    bgcolor: "rgba(3, 237, 165, 0.05)",
                    color: "rgba(27, 83, 249, 1)",
                  },
                }}
              >
                <HistoryIcon />
              </IconButton> */}
              <IconButton
                onClick={handleLogout}
                sx={{
                  color: "#000",
                  ":hover": {
                    bgcolor: "white",
                    color: "rgba(27, 83, 249, 1)",
                  },
                  ":active": {
                    bgcolor: "rgba(3, 237, 165, 0.05)",
                    color: "rgba(27, 83, 249, 1)",
                  },
                }}
              >
                <PowerSettingsNewIcon />
              </IconButton>
            </Box>
          </Toolbar>
        </AppBar>
        <Drawer
          variant="permanent"
          open={open}
          sx={{
            ".MuiDrawer-paperAnchorLeft": {
              borderRight: 0,
              boxShadow: "0px 5px 10px #E7E7E7",
            },
          }}
        >
          <Box
            sx={{
              ":hover": {
                background:
                  "linear-gradient(279.04deg, rgba(3, 237, 165, 0.05) -8.15%, rgba(27, 86, 244, 0.05) 107.52%);",
              },
              borderRadius: 2.5,
            }}
          >
            <Toolbar
              sx={{
                display: "flex",
                justifyContent: "center",
                px: [1],
                py: 3,
              }}
            >
              <Icon
                sx={{
                  height: "29px",
                  width: "174px",
                }}
              >
                <img src={LogoTextIcon} alt="Logo With Text" />
              </Icon>
            </Toolbar>
          </Box>
          <Divider sx={{ width: "75%", mx: "auto" }} />
          <Box
            display={"flex"}
            flexDirection="column"
            height={"100%"}
            justifyContent={"space-between"}
          >
            <Box>
              <List
                component="nav"
                sx={{
                  px: open && 4,
                  py: 4,
                  display: "flex",
                  justifyContent: "center",
                  flexDirection: "column",
                  alignItems: !open && "center",
                }}
              >
                <ListItemButton
                  selected={pathname === "/dashboard"}
                  sx={{
                    borderRadius: 10,
                    px: open ? 2 : 0,
                    ":hover": {
                      bgcolor: "white",
                      color: "rgba(27, 83, 249, 1)",
                    },
                    "&.Mui-selected": {
                      bgcolor: "rgba(3, 237, 165, 0.05)",
                      color: "rgba(27, 83, 249, 1)",
                    },
                  }}
                >
                  <ListItemIcon
                    sx={{
                      display: "flex",
                      justifyContent: !open && "center",
                      color: "inherit",
                    }}
                  >
                    <DashboardOutlinedIcon sx={{ color: "inherit" }} />
                  </ListItemIcon>
                  {open && <ListItemText primary="Dashboard" />}
                </ListItemButton>
                {/* <ListItemButton
                  sx={{
                    borderRadius: 10,
                    px: open ? 2 : 0,
                    ":hover": {
                      bgcolor: "white",
                      color: "rgba(27, 83, 249, 1)",
                    },
                    ":active": {
                      bgcolor: "rgba(3, 237, 165, 0.05)",
                      color: "rgba(27, 83, 249, 1)",
                    },
                  }}
                >
                  <ListItemIcon
                    sx={{
                      display: "flex",
                      justifyContent: !open && "center",
                      color: "inherit",
                    }}
                  >
                    <ConstructionIcon />
                  </ListItemIcon>
                  {open && <ListItemText primary="Equipments" />}
                </ListItemButton>
                <ListItemButton
                  sx={{
                    borderRadius: 10,
                    px: open ? 2 : 0,
                    ":hover": {
                      bgcolor: "white",
                      color: "rgba(27, 83, 249, 1)",
                    },
                    ":active": {
                      bgcolor: "rgba(3, 237, 165, 0.05)",
                      color: "rgba(27, 83, 249, 1)",
                    },
                  }}
                >
                  <ListItemIcon
                    sx={{
                      display: "flex",
                      justifyContent: !open && "center",
                      color: "inherit",
                    }}
                  >
                    <GroupsOutlinedIcon />
                  </ListItemIcon>
                  {open && <ListItemText primary="People" />}
                </ListItemButton> */}
              </List>
              <Divider sx={{ width: "75%", mx: "auto" }} />
            </Box>
            <List component="nav" sx={{ pb: 0, px: open && 4 }}>
              <ListItem
                sx={{
                  flexDirection: "row-reverse",
                  pr: open ? 0 : 2,
                }}
              >
                <IconButton
                  onClick={toggleDrawer}
                  sx={{
                    ":hover": {
                      color: "rgba(27, 83, 249, 1)",
                      bgcolor: "rgba(3, 237, 165, 0.05)",
                    },
                  }}
                >
                  {open ? <ChevronLeftIcon /> : <ChevronRightIcon />}
                </IconButton>
              </ListItem>
              <Divider sx={{ width: !open ? "75%" : "100%", mx: "auto" }} />
              <Box my={2}>
                <Box
                  display="flex"
                  flexDirection={open ? "row" : "column"}
                  justifyContent="center"
                  alignItems="center"
                >
                  <ListItem>
                    <Avatar sx={{ bgcolor: deepPurple[500] }}>AB</Avatar>
                  </ListItem>
                  {/* 
                  <IconButton>
                    <CreateOutlinedIcon />
                  </IconButton> */}
                </Box>
                {open && (
                  <Box ml={2}>
                    <Typography>XYZ ABC</Typography>
                    <Typography>xyz@abc.com</Typography>
                  </Box>
                )}
              </Box>
            </List>
          </Box>
        </Drawer>
        <Box
          component="main"
          sx={{
            flexGrow: 1,
            height: "100vh",
            overflow: "auto",
            bgcolor: "#F5F5F5",
          }}
        >
          <Toolbar />
          <Box my={3} mx={4}>
            <Box display="flex" justifyContent="space-between">
              <Box
                p="1%"
                mb="1%"
                width="43%"
                bgcolor={"#fff"}
                boxShadow="0px 0px 5px rgba(176, 176, 176, 0.1)"
                borderRadius={5}
              >
                <Box display="flex" gap={3} alignItems="start">
                  <Box position="relative">
                    <BoltIcon
                      sx={{
                        borderRadius: "50%",
                        bgcolor: "rgba(217, 217, 217, 0.2)",
                        padding: 0.5,
                      }}
                    />
                    {isTotalPowerLoading && (
                      <CircularProgress
                        size={24}
                        sx={{
                          color: "#254DFC",
                          position: "absolute",
                          top: 0,
                          left: 0,
                          zIndex: 1,
                        }}
                      />
                    )}
                  </Box>
                  <Box width="100%">
                    <Box display="flex" gap={1}>
                      <Typography
                        fontSize={14}
                        fontWeight={500}
                        color="rgba(0, 0, 0, 0.7)"
                      >
                        Total Power (Watt)
                      </Typography>
                      <Tooltip
                        title="Represents the total power consumption trend of a day."
                        arrow
                        placement="right"
                      >
                        <PriorityHighIcon
                          fontSize="12px"
                          sx={{
                            borderRadius: "50%",
                            bgcolor: "#E6E6E6",
                            padding: 0.2,
                            color: "white",
                          }}
                        />
                      </Tooltip>
                    </Box>
                    <Typography fontSize={11} fontWeight={300} color="#8D8D8D">
                      {`as of ${new Date().toLocaleString("default", {
                        month: "long",
                        day: "numeric",
                        year: "numeric",
                        hour: "2-digit",
                        minute: "2-digit",
                        hour12: true,
                      })}`}
                    </Typography>
                    <Box display="flex" justifyContent="space-between" my={2}>
                      <Box>
                        <Box display="flex" gap={1}>
                          <Typography
                            fontSize={24}
                            fontWeight={700}
                            color="#000000"
                          >
                            {totalPower?.split(".")[0]}
                            <Typography
                              component="span"
                              color="rgba(0, 0, 0, 0.5)"
                            >
                              .{totalPower?.split(".")[1]}
                            </Typography>
                            {/* 100
                            <Typography
                              component="span"
                              color="rgba(0, 0, 0, 0.5)"
                            >
                              .33 kW
                            </Typography> */}
                          </Typography>
                          <Tooltip
                            title="Represents live power consumption in comparison to total rated power."
                            arrow
                            placement="right"
                          >
                            <PriorityHighIcon
                              fontSize="12px"
                              sx={{
                                borderRadius: "50%",
                                bgcolor: "#E6E6E6",
                                padding: 0.2,
                                color: "white",
                              }}
                            />
                          </Tooltip>
                        </Box>
                      </Box>
                      <Box>
                        {/* <Box display="flex" gap={1}>
                          <Typography
                            fontSize={24}
                            fontWeight={700}
                            color="#000000"
                          >
                            55
                            <Typography
                              component="span"
                              color="rgba(0, 0, 0, 0.5)"
                            >
                              kWh
                            </Typography>
                          </Typography>
                          <Box>
                            <Typography
                              color="#FF4438"
                              fontWeight={400}
                              fontSize={10}
                            >
                              +10%
                              <TrendingUpIcon fontSize="10px" />
                            </Typography>
                            <Typography
                              color="#8D8D8D"
                              fontWeight={300}
                              fontSize={8}
                            >
                              vs previous day
                            </Typography>
                          </Box>
                          <Tooltip
                            title="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do"
                            arrow
                            placement="right"
                          >
                            <PriorityHighIcon
                              fontSize="12px"
                              sx={{
                                borderRadius: "50%",
                                bgcolor: "#E6E6E6",
                                padding: 0.2,
                                color: "white",
                              }}
                            />
                          </Tooltip>
                        </Box> */}
                      </Box>
                      {/* <Box display="flex" gap={1}>
                        <DownloadIcon fontSize="24px" opacity={"30%"} />
                        <OpenInFullIcon fontSize="24px" opacity={"30%"} />
                      </Box> */}
                    </Box>
                  </Box>
                </Box>
                <ResponsiveContainer width="100%" height="70%">
                  <AreaChart
                    data={areaData}
                    margin={{
                      top: 5,
                      right: 30,
                      left: 20,
                      bottom: 5,
                    }}
                  >
                    <defs>
                      <linearGradient
                        id="colorAreaChart"
                        x1="0"
                        y1="0"
                        x2="0"
                        y2="1"
                      >
                        <stop
                          offset="5%"
                          stopColor="rgba(151, 27, 249, 1)"
                          stopOpacity={0.1}
                        />
                        <stop
                          offset="95%"
                          stopColor="rgba(151, 27, 249, 0.09)"
                          stopOpacity={0.1}
                        />
                      </linearGradient>
                    </defs>
                    <XAxis
                      dataKey="name"
                      fontSize={14}
                      fontWeight={400}
                      tick={{ fill: "#BEBEBE" }}
                      interval={2}
                      tickLine={false}
                      axisLine={false}
                    >
                      <Label
                        offset={-4}
                        value="Time"
                        position="insideBottom"
                        fontSize={11}
                        fill="#616161"
                        fontWeight={300}
                      />
                    </XAxis>
                    <YAxis
                      fontSize={14}
                      fontWeight={400}
                      tick={{ fill: "#971BF9" }}
                      tickLine={false}
                      axisLine={false}
                      label={{
                        value: "Power in Kilo-Watt (kW)",
                        angle: -90,
                        position: "insideLeft",
                        fontWeight: 300,
                        fontSize: 11,
                        offset: -4,
                        fill: "#616161",
                      }}
                    />
                    <ReChartToolTip
                      wrapperStyle={{ outline: "none" }}
                      cursor={false}
                      content={<CustomToolTipOfTotalPower />}
                    />
                    <CartesianGrid vertical={false} stroke="#DDD" />
                    <Tooltip />
                    <Area
                      type="monotone"
                      dataKey="w"
                      stroke="rgba(151, 27, 249, 1)"
                      fillOpacity={1}
                      fill="url(#colorAreaChart)"
                    />
                  </AreaChart>
                </ResponsiveContainer>
              </Box>
              <Box
                p="1%"
                mb="1%"
                width="55%"
                bgcolor={"#fff"}
                boxShadow="0px 0px 5px rgba(176, 176, 176, 0.1)"
                borderRadius={5}
              >
                <Box display="flex" gap={3} alignItems="start">
                  <Box sx={{ position: "relative" }}>
                    <QueryStatsIcon
                      sx={{
                        borderRadius: "50%",
                        bgcolor: "rgba(217, 217, 217, 0.2)",
                        padding: 0.5,
                      }}
                    />
                    {isPowerConsumptionLoading && (
                      <CircularProgress
                        size={24}
                        sx={{
                          color: "#254DFC",
                          position: "absolute",
                          top: 0,
                          left: 0,
                          zIndex: 1,
                        }}
                      />
                    )}
                  </Box>
                  <Box width="100%">
                    <Box display="flex" justifyContent="space-between">
                      <Box>
                        <Box display="flex" gap={1}>
                          <Typography
                            fontSize={14}
                            fontWeight={500}
                            color="rgba(0, 0, 0, 0.7)"
                          >
                            Consumption Pattern
                          </Typography>
                          <Tooltip
                            title="Represents the unit consumption trend of a month/year."
                            arrow
                            placement="right"
                          >
                            <PriorityHighIcon
                              fontSize="12px"
                              sx={{
                                borderRadius: "50%",
                                bgcolor: "#E6E6E6",
                                padding: 0.2,
                                color: "white",
                              }}
                            />
                          </Tooltip>
                        </Box>
                        <Typography
                          fontSize={11}
                          fontWeight={300}
                          color="#8D8D8D"
                        >
                          {`as of ${new Date().toLocaleString("default", {
                            month: "long",
                            day: "numeric",
                            year: "numeric",
                          })}`}
                        </Typography>
                      </Box>
                      <Box display="flex">
                        <FormControl sx={{ m: 1, minWidth: 80 }} size="small">
                          <Select
                            displayEmpty
                            value={year}
                            onChange={(e) => setYear(e?.target?.value)}
                            input={
                              <OutlinedInput
                                sx={{
                                  border: "1px solid rgba(0, 0, 0, 0.1)",
                                  borderRadius: "14.5px",
                                  "& fieldset": { border: "none" },
                                  color: "#7F7F7F",
                                  fontSize: "14px",
                                  fontWeight: 500,
                                  "& .MuiSvgIcon-root": {
                                    color: "#7F7F7F",
                                  },
                                  "& em": {
                                    fontStyle: "normal",
                                  },
                                }}
                              />
                            }
                            renderValue={(selected) => {
                              if (!selected) {
                                return <em>Year</em>;
                              }
                              return selected;
                            }}
                            MenuProps={{
                              PaperProps: {
                                style: {
                                  maxHeight: 48 * 4.5 + 8,
                                  width: 250,
                                },
                              },
                            }}
                            inputProps={{ "aria-label": "Without label" }}
                          >
                            {years(2019 - 20).map((yearOption) => (
                              <MenuItem key={yearOption} value={yearOption}>
                                {yearOption}
                              </MenuItem>
                            ))}
                          </Select>
                        </FormControl>
                        <FormControl sx={{ m: 1, minWidth: 80 }} size="small">
                          <Select
                            displayEmpty
                            value={month}
                            onChange={(e) => setMonth(e?.target?.value)}
                            input={
                              <OutlinedInput
                                sx={{
                                  border: "1px solid rgba(0, 0, 0, 0.1)",
                                  borderRadius: "14.5px",
                                  "& fieldset": { border: "none" },
                                  color: "#7F7F7F",
                                  fontSize: "14px",
                                  fontWeight: 500,
                                  "& .MuiSvgIcon-root": {
                                    color: "#7F7F7F",
                                  },
                                  "& em": {
                                    fontStyle: "normal",
                                  },
                                }}
                              />
                            }
                            renderValue={(selected) => {
                              if (!selected) {
                                return <em>Month</em>;
                              }
                              return selected;
                            }}
                            MenuProps={{
                              PaperProps: {
                                style: {
                                  maxHeight: 48 * 4.5 + 8,
                                  width: 250,
                                },
                              },
                            }}
                            inputProps={{ "aria-label": "Without label" }}
                          >
                            {monthNames.map((monthSelected) => (
                              <MenuItem
                                key={monthSelected}
                                value={monthSelected}
                              >
                                {monthSelected}
                              </MenuItem>
                            ))}
                          </Select>
                        </FormControl>
                      </Box>
                    </Box>
                    <Box display="flex" justifyContent="space-between" my={2}>
                      <Box>
                        <Box display="flex" gap={1}>
                          <Typography
                            fontSize={24}
                            fontWeight={700}
                            color="#000000"
                          >
                            {totalPowerConsumption?.split(".")[0]}
                            <Typography
                              component="span"
                              color="rgba(0, 0, 0, 0.5)"
                            >
                              .{totalPowerConsumption?.split(".")[1]}
                            </Typography>
                          </Typography>
                          {/* <Box>
                            <Typography
                              color="#07D2B4"
                              fontWeight={400}
                              fontSize={10}
                            >
                              -7%
                              <TrendingDownIcon fontSize="10px" />
                            </Typography>
                            <Typography
                              color="#8D8D8D"
                              fontWeight={300}
                              fontSize={8}
                            >
                              vs previous 30 days
                            </Typography>
                          </Box> */}
                          <Tooltip
                            title="Represents the total units consumption of a month/year."
                            arrow
                            placement="right"
                          >
                            <PriorityHighIcon
                              fontSize="12px"
                              sx={{
                                borderRadius: "50%",
                                bgcolor: "#E6E6E6",
                                padding: 0.2,
                                color: "white",
                              }}
                            />
                          </Tooltip>
                        </Box>
                      </Box>
                      {/* <Box display="flex" gap={1}>
                        <DownloadIcon fontSize="24px" opacity={"30%"} />
                        <OpenInFullIcon fontSize="24px" opacity={"30%"} />
                      </Box> */}
                    </Box>
                  </Box>
                </Box>
                <ResponsiveContainer width="100%" height="70%">
                  <BarChart
                    data={monthData}
                    margin={{
                      top: 5,
                      right: 30,
                      left: 20,
                      bottom: 5,
                    }}
                  >
                    <defs>
                      <linearGradient
                        id="colorBarChart"
                        x1="0"
                        y1="0"
                        x2="0"
                        y2="100%"
                        gradientUnits="userSpaceOnUse"
                      >
                        <stop offset="0" stopColor="#1D47FF" />
                        <stop offset="1" stopColor="#04E3AA" />
                      </linearGradient>
                    </defs>
                    <XAxis
                      dataKey="name"
                      fontSize={14}
                      fontWeight={400}
                      tick={{ fill: "#BEBEBE" }}
                      tickLine={false}
                      axisLine={false}
                    >
                      <Label
                        offset={-4}
                        value={`${month} ${year}`}
                        position="insideBottom"
                        fontSize={11}
                        fill="#616161"
                        fontWeight={300}
                      />
                    </XAxis>
                    <YAxis
                      fontSize={14}
                      fontWeight={400}
                      tick={{ fill: "#464646" }}
                      tickLine={false}
                      axisLine={false}
                      label={{
                        value: "Power Consumption (kW)",
                        angle: -90,
                        position: "insideLeft",
                        fontWeight: 300,
                        fontSize: 11,
                        offset: -16,
                        fill: "#616161",
                      }}
                    />
                    <ReChartToolTip
                      wrapperStyle={{ outline: "none" }}
                      cursor={false}
                      content={
                        <CustomToolTipOfConsumptionPattern color="#1770EA" />
                      }
                    />
                    <CartesianGrid
                      vertical={false}
                      stroke="rgba(29, 33, 37, 0.4)"
                      strokeOpacity={0.2}
                    />
                    <Bar
                      dataKey="w"
                      fill="url(#colorBarChart)"
                      barSize={9}
                      radius={[10, 10, 0, 0]}
                    />
                  </BarChart>
                </ResponsiveContainer>
              </Box>
            </Box>
          </Box>
          {/* <Box my={3} mx={4}>
            <Box display="flex" justifyContent="space-between" height="100%">
              <Box
                p="1%"
                mb="1%"
                width="19%"
                bgcolor={"#fff"}
                boxShadow="0px 0px 5px rgba(176, 176, 176, 0.1)"
                borderRadius={5}
              >
                <Box
                  display="flex"
                  gap={1}
                  alignItems="start"
                  flexDirection="column"
                >
                  <Box display="flex" gap={1}>
                    <BoltIcon
                      sx={{
                        borderRadius: "50%",
                        bgcolor: "rgba(217, 217, 217, 0.2)",
                        padding: 0.5,
                      }}
                    />
                    <Box display="flex" gap={1}>
                      <Typography
                        fontSize={14}
                        fontWeight={500}
                        color="rgba(0, 0, 0, 0.7)"
                      >
                        Current (A)
                      </Typography>
                      <Tooltip
                        title="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do"
                        arrow
                        placement="right"
                      >
                        <PriorityHighIcon
                          fontSize="12px"
                          sx={{
                            borderRadius: "50%",
                            bgcolor: "#E6E6E6",
                            padding: 0.2,
                            color: "white",
                          }}
                        />
                      </Tooltip>
                    </Box>
                  </Box>
                  <Box display="flex" justifyContent="space-between">
                    <Box display="flex" gap={1}>
                      <Typography
                        fontSize={24}
                        fontWeight={700}
                        color="#000000"
                      >
                        {totalCurrent?.split(".")[0]}
                        <Typography component="span" color="rgba(0, 0, 0, 0.5)">
                          .{totalCurrent?.split(".")[1]}
                        </Typography>
                      </Typography>
                      <Box>
                        <Typography
                          color="#FF4438"
                          fontWeight={400}
                          fontSize={10}
                        >
                          +10%
                        </Typography>
                        <Typography
                          color="#8D8D8D"
                          fontWeight={300}
                          fontSize={8}
                        >
                          vs previous day
                        </Typography>
                      </Box>
                    </Box>
                  </Box>
                </Box>
                <Box width="100%" height="70%">
                  <ResponsiveContainer width="100%" height="100%">
                    <BarChart data={smallData1}>
                      <defs>
                        <linearGradient
                          id="colorBarChart1"
                          x1="0"
                          y1="0"
                          x2="0"
                          y2="100%"
                          gradientUnits="userSpaceOnUse"
                        >
                          <stop
                            offset="0"
                            stopColor="rgba(151, 27, 249, 0.2)"
                          />
                          <stop offset="1" stopColor="rgba(151, 27, 249, 1)" />
                        </linearGradient>
                      </defs>
                      <XAxis
                        dataKey="name"
                        fontSize={10}
                        fontWeight={400}
                        tick={{ fill: "#BEBEBE" }}
                        tickLine={false}
                        axisLine={false}
                      >
                        <Label
                          offset={1}
                          value="min."
                          position="insideBottomLeft"
                          fontSize={10}
                          fill="#9F2CFA"
                          fontWeight={500}
                        />
                        <Label
                          offset={1}
                          value="max."
                          position="insideBottomRight"
                          fontSize={10}
                          fill="#9F2DFA"
                          fontWeight={500}
                        />
                      </XAxis>
                      <ReChartToolTip
                        wrapperStyle={{ outline: "none" }}
                        cursor={false}
                        content={
                          <CustomToolTipOfConsumptionPattern color="#971BF9" />
                        }
                      />
                      <CartesianGrid
                        vertical={false}
                        stroke="rgba(29, 33, 37, 0.4)"
                        strokeOpacity={0.2}
                      />
                      <Bar
                        dataKey="pv"
                        fill="url(#colorBarChart1)"
                        barSize={8}
                        radius={[10, 10, 0, 0]}
                      />
                    </BarChart>
                  </ResponsiveContainer>
                </Box>
              </Box>
              <Box
                p="1%"
                mb="1%"
                width="19%"
                bgcolor={"#fff"}
                boxShadow="0px 0px 5px rgba(176, 176, 176, 0.1)"
                borderRadius={5}
              >
                <Box
                  display="flex"
                  gap={1}
                  alignItems="start"
                  flexDirection="column"
                >
                  <Box display="flex" gap={1}>
                    <BatteryCharging20Icon
                      sx={{
                        borderRadius: "50%",
                        bgcolor: "rgba(217, 217, 217, 0.2)",
                        padding: 0.5,
                      }}
                    />
                    <Box display="flex" gap={1}>
                      <Typography
                        fontSize={14}
                        fontWeight={500}
                        color="rgba(0, 0, 0, 0.7)"
                      >
                        Power Factor
                      </Typography>
                      <Tooltip
                        title="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do"
                        arrow
                        placement="right"
                      >
                        <PriorityHighIcon
                          fontSize="12px"
                          sx={{
                            borderRadius: "50%",
                            bgcolor: "#E6E6E6",
                            padding: 0.2,
                            color: "white",
                          }}
                        />
                      </Tooltip>
                    </Box>
                  </Box>
                  <Box display="flex" justifyContent="space-between">
                    <Box display="flex" gap={1}>
                      <Typography
                        fontSize={24}
                        fontWeight={700}
                        color="#FF4438"
                      >
                        {getPowerFactor}
                      </Typography>
                      <Box display="flex" alignItems="center">
                        <Typography
                          color="#8D8D8D"
                          fontWeight={300}
                          fontSize={8}
                        >
                          Average Value
                        </Typography>
                      </Box>
                    </Box>
                  </Box>
                </Box>
                <Box width="100%" height="70%">
                  <ResponsiveContainer width="100%" height="100%">
                    <BarChart data={smallData2}>
                      <defs>
                        <linearGradient
                          id="colorBarChart2"
                          x1="0"
                          y1="0"
                          x2="0"
                          y2="100%"
                          gradientUnits="userSpaceOnUse"
                        >
                          <stop offset="0" stopColor="rgba(255, 68, 56, 0.5)" />
                          <stop offset="1" stopColor="rgba(255, 68, 56, 1)" />
                        </linearGradient>
                      </defs>
                      <XAxis
                        dataKey="name"
                        fontSize={10}
                        fontWeight={400}
                        tick={{ fill: "#808080" }}
                        tickLine={false}
                        axisLine={false}
                      >
                        <Label
                          offset={1}
                          value="min."
                          position="insideBottomLeft"
                          fontSize={10}
                          fill="#07D7B1"
                          fontWeight={500}
                        />
                        <Label
                          offset={1}
                          value="max."
                          position="insideBottomRight"
                          fontSize={10}
                          fill="#FF4438"
                          fontWeight={500}
                        />
                      </XAxis>
                      <ReChartToolTip
                        wrapperStyle={{ outline: "none" }}
                        cursor={false}
                        content={
                          <CustomToolTipOfConsumptionPattern color="#FF4539" />
                        }
                      />
                      <CartesianGrid
                        vertical={false}
                        stroke="rgba(29, 33, 37, 0.4)"
                        strokeOpacity={0.2}
                      />
                      <Bar
                        dataKey="pv"
                        fill="url(#colorBarChart2)"
                        barSize={8}
                        radius={[10, 10, 0, 0]}
                      />
                    </BarChart>
                  </ResponsiveContainer>
                </Box>
              </Box>
              <Box
                p="1%"
                mb="1%"
                width="19%"
                bgcolor={"#fff"}
                boxShadow="0px 0px 5px rgba(176, 176, 176, 0.1)"
                borderRadius={5}
              >
                <Box
                  display="flex"
                  gap={1}
                  alignItems="start"
                  flexDirection="column"
                >
                  <Box display="flex" gap={1}>
                    <MonitorHeartIcon
                      sx={{
                        borderRadius: "50%",
                        bgcolor: "rgba(217, 217, 217, 0.2)",
                        padding: 0.5,
                      }}
                    />
                    <Box display="flex" gap={1}>
                      <Typography
                        fontSize={14}
                        fontWeight={500}
                        color="rgba(0, 0, 0, 0.7)"
                      >
                        THD
                      </Typography>
                      <Tooltip
                        title="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do"
                        arrow
                        placement="right"
                      >
                        <PriorityHighIcon
                          fontSize="12px"
                          sx={{
                            borderRadius: "50%",
                            bgcolor: "#E6E6E6",
                            padding: 0.2,
                            color: "white",
                          }}
                        />
                      </Tooltip>
                    </Box>
                  </Box>
                  <Box display="flex" justifyContent="space-between">
                    <Box display="flex" gap={1}>
                      <Typography
                        fontSize={24}
                        fontWeight={700}
                        color="#FF4438"
                      >
                        25%
                      </Typography>
                      <Box display="flex" alignItems="center">
                        <Typography
                          color="#8D8D8D"
                          fontWeight={300}
                          fontSize={8}
                        >
                          Average Value
                        </Typography>
                      </Box>
                    </Box>
                  </Box>
                </Box>
                <Box width="100%" height="70%">
                  <ResponsiveContainer width="100%" height="100%">
                    <BarChart data={smallData3}>
                      <defs>
                        <linearGradient
                          id="colorBarChart2"
                          x1="0"
                          y1="0"
                          x2="0"
                          y2="100%"
                          gradientUnits="userSpaceOnUse"
                        >
                          <stop offset="0" stopColor="rgba(255, 68, 56, 0.5)" />
                          <stop offset="1" stopColor="rgba(255, 68, 56, 1)" />
                        </linearGradient>
                      </defs>
                      <XAxis
                        dataKey="name"
                        fontSize={10}
                        fontWeight={400}
                        tick={{ fill: "#808080" }}
                        tickLine={false}
                        axisLine={false}
                      >
                        <Label
                          offset={1}
                          value="min."
                          position="insideBottomLeft"
                          fontSize={10}
                          fill="#07D7B1"
                          fontWeight={500}
                        />
                        <Label
                          offset={1}
                          value="max."
                          position="insideBottomRight"
                          fontSize={10}
                          fill="#FF4438"
                          fontWeight={500}
                        />
                      </XAxis>
                      <ReChartToolTip
                        wrapperStyle={{ outline: "none" }}
                        cursor={false}
                        content={
                          <CustomToolTipOfConsumptionPattern color="#FF4539" />
                        }
                      />
                      <CartesianGrid
                        vertical={false}
                        stroke="rgba(29, 33, 37, 0.4)"
                        strokeOpacity={0.2}
                      />
                      <Bar
                        dataKey="pv"
                        fill="url(#colorBarChart2)"
                        barSize={8}
                        radius={[10, 10, 0, 0]}
                      />
                    </BarChart>
                  </ResponsiveContainer>
                </Box>
              </Box>
              <Box
                p="1%"
                mb="1%"
                width="19%"
                bgcolor={"#fff"}
                boxShadow="0px 0px 5px rgba(176, 176, 176, 0.1)"
                borderRadius={5}
              >
                <Box
                  display="flex"
                  gap={1}
                  alignItems="start"
                  flexDirection="column"
                >
                  <Box display="flex" gap={1}>
                    <GraphicEqIcon
                      sx={{
                        borderRadius: "50%",
                        bgcolor: "rgba(217, 217, 217, 0.2)",
                        padding: 0.5,
                      }}
                    />
                    <Box display="flex" gap={1}>
                      <Typography
                        fontSize={14}
                        fontWeight={500}
                        color="rgba(0, 0, 0, 0.7)"
                      >
                        Frequency
                      </Typography>
                      <Tooltip
                        title="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do"
                        arrow
                        placement="right"
                      >
                        <PriorityHighIcon
                          fontSize="12px"
                          sx={{
                            borderRadius: "50%",
                            bgcolor: "#E6E6E6",
                            padding: 0.2,
                            color: "white",
                          }}
                        />
                      </Tooltip>
                    </Box>
                  </Box>
                  <Box display="flex" justifyContent="space-between">
                    <Box display="flex" gap={1}>
                      <Typography
                        fontSize={24}
                        fontWeight={700}
                        color="#07D7B1"
                      >
                        {getFrequency}
                      </Typography>
                      <Box display="flex" alignItems="center">
                        <Typography
                          color="#8D8D8D"
                          fontWeight={300}
                          fontSize={8}
                        >
                          Average Value
                        </Typography>
                      </Box>
                    </Box>
                  </Box>
                </Box>
                <Box width="100%" height="70%">
                  <ResponsiveContainer width="100%" height="100%">
                    <BarChart data={smallData2}>
                      <defs>
                        <linearGradient
                          id="colorBarChart5"
                          x1="0"
                          y1="0"
                          x2="0"
                          y2="100%"
                          gradientUnits="userSpaceOnUse"
                        >
                          <stop offset="0" stopColor="rgba(5, 222, 173, 1)" />
                          <stop offset="1" stopColor="rgba(5, 222, 173, 0.5)" />
                        </linearGradient>
                      </defs>
                      <XAxis
                        dataKey="name"
                        fontSize={10}
                        fontWeight={400}
                        tick={{ fill: "#808080" }}
                        tickLine={false}
                        axisLine={false}
                      >
                        <Label
                          offset={1}
                          value="min."
                          position="insideBottomLeft"
                          fontSize={10}
                          fill="#07D7B1"
                          fontWeight={500}
                        />
                        <Label
                          offset={1}
                          value="max."
                          position="insideBottomRight"
                          fontSize={10}
                          fill="#FF4438"
                          fontWeight={500}
                        />
                      </XAxis>
                      <ReChartToolTip
                        wrapperStyle={{ outline: "none" }}
                        cursor={false}
                        content={
                          <CustomToolTipOfConsumptionPattern color="#05DEAD" />
                        }
                      />
                      <CartesianGrid
                        vertical={false}
                        stroke="rgba(29, 33, 37, 0.4)"
                        strokeOpacity={0.2}
                      />
                      <Bar
                        dataKey="pv"
                        fill="url(#colorBarChart5)"
                        barSize={8}
                        radius={[10, 10, 0, 0]}
                      />
                    </BarChart>
                  </ResponsiveContainer>
                </Box>
              </Box>
              <Box
                p="1%"
                mb="1%"
                width="19%"
                bgcolor={"#fff"}
                boxShadow="0px 0px 5px rgba(176, 176, 176, 0.1)"
                borderRadius={5}
              >
                <Box
                  display="flex"
                  gap={1}
                  alignItems="start"
                  flexDirection="column"
                >
                  <Box display="flex" gap={1}>
                    <ElectricMeterIcon
                      sx={{
                        borderRadius: "50%",
                        bgcolor: "rgba(217, 217, 217, 0.2)",
                        padding: 0.5,
                      }}
                    />
                    <Box display="flex" gap={1}>
                      <Typography
                        fontSize={14}
                        fontWeight={500}
                        color="rgba(0, 0, 0, 0.7)"
                      >
                        Voltage
                      </Typography>
                      <Tooltip
                        title="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do"
                        arrow
                        placement="right"
                      >
                        <PriorityHighIcon
                          fontSize="12px"
                          sx={{
                            borderRadius: "50%",
                            bgcolor: "#E6E6E6",
                            padding: 0.2,
                            color: "white",
                          }}
                        />
                      </Tooltip>
                    </Box>
                  </Box>
                  <Box display="flex" justifyContent="space-between">
                    <Box display="flex" gap={1}>
                      <Typography
                        fontSize={24}
                        fontWeight={700}
                        color="#07D7B1"
                      >
                        230V
                      </Typography>
                      <Box display="flex" alignItems="center">
                        <Typography
                          color="#8D8D8D"
                          fontWeight={300}
                          fontSize={8}
                        >
                          Average Value
                        </Typography>
                      </Box>
                    </Box>
                  </Box>
                </Box>
                <Box width="100%" height="70%">
                  <ResponsiveContainer width="100%" height="100%">
                    <BarChart data={smallData3}>
                      <defs>
                        <linearGradient
                          id="colorBarChart5"
                          x1="0"
                          y1="0"
                          x2="0"
                          y2="100%"
                          gradientUnits="userSpaceOnUse"
                        >
                          <stop offset="0" stopColor="rgba(5, 222, 173, 1)" />
                          <stop offset="1" stopColor="rgba(5, 222, 173, 0.5)" />
                        </linearGradient>
                      </defs>
                      <XAxis
                        dataKey="name"
                        fontSize={10}
                        fontWeight={400}
                        tick={{ fill: "#808080" }}
                        tickLine={false}
                        axisLine={false}
                      >
                        <Label
                          offset={1}
                          value="min."
                          position="insideBottomLeft"
                          fontSize={10}
                          fill="#07D7B1"
                          fontWeight={500}
                        />
                        <Label
                          offset={1}
                          value="max."
                          position="insideBottomRight"
                          fontSize={10}
                          fill="#FF4438"
                          fontWeight={500}
                        />
                      </XAxis>
                      <ReChartToolTip
                        wrapperStyle={{ outline: "none" }}
                        cursor={false}
                        content={
                          <CustomToolTipOfConsumptionPattern color="#05DEAD" />
                        }
                      />
                      <CartesianGrid
                        vertical={false}
                        stroke="rgba(29, 33, 37, 0.4)"
                        strokeOpacity={0.2}
                      />
                      <Bar
                        dataKey="pv"
                        fill="url(#colorBarChart5)"
                        barSize={8}
                        radius={[10, 10, 0, 0]}
                      />
                    </BarChart>
                  </ResponsiveContainer>
                </Box>
              </Box>
            </Box>
          </Box> */}
        </Box>
      </Box>
    </ThemeProvider>
  );
};

export default Dashboard;
