import React from "react";
import { Navigate } from "react-router-dom";

const ProtectRoute = ({ component: Component, ...props }) => {
  const isLoggedIn = localStorage.getItem("isLoggedIn");
  return isLoggedIn ? <Component {...props} /> : <Navigate to={"/login"} />;
};

export default ProtectRoute;
