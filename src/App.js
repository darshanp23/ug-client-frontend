import "./App.css";
import {
  createBrowserRouter,
  Navigate,
  RouterProvider,
} from "react-router-dom";
import Login from "./containers/Login";
import Dashboard from "./containers/Dashboard";
import ProtectRoute from "./components/ProtectedRoute";

function App() {
  const router = createBrowserRouter([
    {
      path: "/",
      element: <Navigate to={"/dashboard"} />,
    },
    {
      path: "/login",
      element: <Login />,
    },
    {
      path: "/dashboard",
      element: <ProtectRoute component={Dashboard} />,
    },
  ]);

  return <RouterProvider router={router} />;
}

export default App;
